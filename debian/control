Source: ocaml-reins
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml,
 omake,
 libounit-ocaml-dev,
 libnum-ocaml-dev,
 ocaml-findlib,
 dh-ocaml
Standards-Version: 4.6.2
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/ocaml-team/ocaml-reins.git
Vcs-Browser: https://salsa.debian.org/ocaml-team/ocaml-reins
Homepage: http://ocaml-reins.sourceforge.net/

Package: libreins-ocaml-dev
Architecture: any
Depends:
 ${ocaml:Depends},
 ${shlibs:Depends},
 ${misc:Depends}
Provides: ${ocaml:Provides}
Suggests: ocaml-findlib
Description: data structure library for OCaml
 The OCaml Reins data structure library consists of the following
 persistent implementations:
  * Lists (singly, O(1) catenable, Acyclic doubly linked,
           random access)
  * Sets/Maps (AVL, Red/Black, Patricia, Splay)
  * Heaps (Binomial, Skew-Binomial)
 .
 All of the implementations conform to a unified signature for each
 data type.  Also, each data types include zipper style cursor
 interfaces and persistent, bi-directional cursor based iterators.
 The library also includes a set of standard modules to hoist the base
 typs into the module level (Int, Bool, etc...) as well as a
 collection of functor combinators to minimize boilerplate (e.g., for
 constructing compare or to_string functions).  Finally, a
 quickcheck-like random testing framework is included and each data
 type supports the necessary "gen" function to generate a random
 instance of the type.
